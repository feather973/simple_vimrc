" add nerdtree / git-fugitive / taglist / vim-lsp / vim-go
call pathogen#infect('~/.vim_runtime/{}')

" nerdtree setting (open current directory)
set autochdir
let NERDTreeChDirMode=2
autocmd FileType nerdtree nmap <buffer> <CR> go		"enter : open file, o : open dir

" cscope db autoload (current directory)
set csre			" need when cwd has cscope.out
function! LoadCscope()
    let db = findfile("cscope.out", ".;")
    if (!empty(db))
        let path = strpart(db, 0, match(db, "/cscope.out$"))
        set nocscopeverbose " suppress 'duplicate connection' error
	exe "cs add " . db . " " . path
	set cscopeverbose
    endif
endfunction
call LoadCscope()

" tabline
function! SetTabNumber()
    let ret = ''
    for i in range(tabpagenr('$'))
	let tab = i+1
	let winnr = tabpagewinnr(tab)

	let buflist = tabpagebuflist(tab)
	let bufnr = buflist[winnr - 1]
	let bufname = bufname(bufnr)

	" select tab
	let ret .= '%' . tab . 'T'
	let ret .= (tab == tabpagenr() ? '%#TabLineSel#' : '%#TabLine#')

	" print tabnumber
	let ret .= ' ' . tab . ':'
	let ret .= (bufname != '' ? '['. fnamemodify(bufname, ':t') . '] ' : '[No Name] ')
    endfor
    return ret
endfunction
set tabline=%!SetTabNumber()

" ctags autoload
set tags=tags;/

" open file with previous cursor
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview
autocmd FileType vim set shiftwidth=4

" misc
set number			" print line number
set cscopetag			" use ctrl-] , :ta , and vim -t
set csto=0			" search cscope first
set hlsearch			" highlight all search results
set tabstop=4			" tab = 4 space
set shiftwidth=4		" maybe shiftwidth is still 8, so =G will insert 2 tabs(problem) not just 1 tabs..
set cursorline		" highlight cursor line

"""" key maps """"
let mapleader = " "		" <leader> is space

" nerdtree
let toggleflag=0
function! NERDTreeToggleWrapper()
    if (!g:toggleflag)
	NERDTree .		" nerdtree open current file directory
	let g:toggleflag=1
    else
	NERDTreeClose
	let g:toggleflag=0
    endif
endfunction
	
"nnoremap <leader>n :call NERDTreeToggleWrapper()<cr>
nnoremap <C-n> :call NERDTreeToggleWrapper()<cr>

"taglist
function! TagListOpen()
	:TlistToggle
"	call feedkeys("\<C-w>t")		"move cursor to first window
endfunction

"nnoremap <leader>m :call TagListOpen()<CR>
nnoremap <C-g> :call TagListOpen()<CR>


" cscope
nmap s :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap g :cs find g <C-R>=expand("<cword>")<CR><CR>
function! T()
    echo "search string?"
    let search_string = input(':')
"    echo "search string is ".search_string
    exe ':cs find t ' . search_string
endfunction
nmap t :call T()<cr>


" vim-fugitive ( version 3.3 for older vim )
filetype plugin indent on
nnoremap b :Git blame<cr>

" Tab
function! N()
    let cur_line = line(".")
    exe ':tabnew %'
    exe ':'.cur_line
	set noscrollbind
endfunction
map <leader><leader> :call N()<cr>	" open current file in new tab with same cursor

map q :q!<cr>

" Tab move
 for c in range(char2nr('1'), char2nr('9'))
 "    exe 'nnoremap ' . nr2char(c) . ' :echo " Pressed: ' . nr2char(c) . '"<CR>'
     exe 'nnoremap ' . nr2char(c) . ' :tabn ' . nr2char(c) . '<CR>'
 endfor

nnoremap <C-v> :tabprevious<CR>
nnoremap <C-b> :tabnext<CR>

" prev/next word
nnoremap <S-h> b
nnoremap <S-l> w

" Window
nnoremap w :vs<cr> 
nnoremap e :sp<cr>

" Window move
map <C-h> <C-W>h
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-l> <C-W>l

" Language Server Protocol configuration for rs file
autocmd BufRead,BufNewFile *.rs setlocal filetype=rust
au User lsp_setup call lsp#register_server({
    \ 'name': 'rust-analyzer',
    \ 'cmd': {server_info->['rust-analyzer']},
    \ 'whitelist': ['rust'],
    \ })

" vim-lsp for rs file
autocmd BufRead,BufNewFile *.rs nmap g :LspDefinition<CR>
autocmd BufRead,BufNewFile *.rs nmap s :LspReferences<CR>

" vim-go for go file
autocmd BufRead,BufNewFile *.go nmap g :GoDef<CR>
autocmd BufRead,BufNewFile *.go nmap s :GoReferrers<CR>
autocmd BufRead,BufNewFile *.go nmap t :GoImplements<CR>

" Keymap for samsung galaxy S7 tab keyboard
"inoremap ` <Esc>

" lsp debug purpose
"let g:lsp_log_verbose = 1
"let g:lsp_log_file = expand('~/vim-lsp.log')

"""""""""""""""""""

" reference
" :h fillchars		window separator character
" :h expand		expr
" :h cword		word under cursor
" :h <C-R>		save result to register	
" :h CR 		carriage return
" :h char2nr		return first number in expr
" :h range()		range(4) is [0, 1, 2, 3]
" :h call		call function
" :h exe		execute vim command
" :h nnoremap		avoid nested mapping
" :h Ctrl-m		<CR> is ctrl + m, cannot be mapped
" :h input		input prompt
" :h window-move-cursor		window move
" :LspStatus		check lsp server is running
"
" The NERD tree     			https://www.vim.org/scripts/script.php?script_id=1658
" cscope_macros.vim 			https://www.vim.org/scripts/script.php?script_id=51
" cscope autoload			http://www.iamroot.org/xe/index.php?mid=Knowledge&document_srl=14810
" open file with previous cursor	https://stackoverflow.com/a/775918
" open file when cwd has cscope.out	https://stackoverflow.com/a/29604533
" git blame need filetype indent	https://issueexplorer.com/issue/tpope/vim-fugitive/1774
" nerdtree open file in split window	https://github.com/preservim/nerdtree/issues/1123
" tabline				https://github.com/mkitt/tabline.vim/blob/master/plugin/tabline.vim
" shiftwidth				https://vi.stackexchange.com/questions/10152/vim-indent-using-inserts-2-tabs
" rust-analyzer + vim-lsp	https://rust-analyzer.github.io
" 							https://github.com/prabirshrestha/vim-lsp
" lsp debugging				https://github.com/prabirshrestha/vim-lsp?tab=readme-ov-file#debugging
